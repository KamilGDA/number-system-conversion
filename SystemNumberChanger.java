import java.util.Scanner;

public class SystemNumberChanger {

    public static void main(String[] args) {

        String DecimalInputPL = "(PL)Wpisz liczbę w systemie dziesiętnym";
        String DecimalInputEN = "(EN)Write a number in decimal system";

        String BinaryInputPL = "(PL)Wpisz liczbę w systemie binarnym";
        String BinaryInputEN = "(EN)Write a number in binary system";

        String OctalInputPL = "(PL)Wpisz liczbę w systemie ósemkowym";
        String OctalInputEN = "(EN)Write a number in octal system";

        String HexaInputPL = "(PL)Wpisz liczbę w systemie szesnastkowym";
        String HexaInputEN = "(EN)Write a number in hexadecimal system";

        String ConvertedPL = "(PL)Podana liczba po konwersji na system";
        String ConvertedEN = "(EN)Number you entered after conversion to";

        System.out.println("(PL)Konwersja systemów liczbowych");
        System.out.println("(PL)Z jakiego systemu chcesz przeliczyć?");
        System.out.println("(EN)Number System Conversion (NSC)");
        System.out.println("(EN)From which number system you want to convert?");
        System.out.println(" 1.Dziesiętnego / Decimal \n 2.Binarnego / Binary (0-1) \n " +
                "3.Ósemkowego / Octal (0-8) \n 4.Szesnastkowego / Hexadecimal (0-16)");
        Scanner input = new Scanner(System.in);
        String system = input.nextLine();
        if (system.matches("1")) {

            System.out.println("(PL)Wybrałeś przeliczenie liczby z systemu dziesiętnego, na jaki system chcesz przeliczyć?");
            System.out.println("(EN)You choose to convert number from decimal. To which system you want to convert?");

            System.out.println(" 1.Binarnego / Binary (0-1)  \n 2.Ósemkowego / Octal (0-8) " +
                    "\n 3.Szesnastkowego / Hexadecimal (0-16)");

            String inp = input.nextLine();

            if (inp.matches("1")) {

                System.out.println(DecimalInputPL);
                System.out.println(DecimalInputEN);

                String userInput = input.nextLine();

                int number = Integer.parseInt(userInput);

                System.out.println(ConvertedPL + " binarny:");
                System.out.println(ConvertedEN + " binary system:");
                System.out.println(Integer.toBinaryString(number));

            } else if (inp.matches("2")) {

                System.out.println(DecimalInputPL);
                System.out.println(DecimalInputEN);

                String in = input.nextLine();

                int number = Integer.parseInt(in);

                System.out.println(ConvertedPL + " ósemkowy:");
                System.out.println(ConvertedEN + " octal system:");
                System.out.println(Integer.toOctalString(number));

            } else if (inp.matches("3")) {

                System.out.println(DecimalInputPL);
                System.out.println(DecimalInputEN);

                String in = input.nextLine();

                int number = Integer.parseInt(in);

                System.out.println(ConvertedPL + " szesnastkowy:");
                System.out.println(ConvertedEN + " hexadecimal system:");
                System.out.println(Integer.toHexString(number));
            }

        } else if (system.matches("2")) {

            System.out.println("(PL)Wybrałeś przeliczenie liczby z systemu binarnego, na jaki system chcesz przeliczyć?");
            System.out.println("(EN)You choose to convert number from binary. To which system you want to convert?");

            System.out.println(" 1.Dziesiętny / Decimal  \n 2.Ósemkowy / Octal (0-8) \n 3.Szesnastkowy / Hexadecimal (0-16)");

            String inp = input.nextLine();

            if (inp.matches("1")) {

                System.out.println(BinaryInputPL);
                System.out.println(BinaryInputEN);

                String in = input.nextLine();

                System.out.println(ConvertedPL + " dziesiętny:");
                System.out.println(ConvertedEN + " decimal system:");

                System.out.println(Integer.parseInt(in, 2));

            } else if (inp.matches("2")) {

                System.out.println(BinaryInputPL+" \n"+BinaryInputEN);

                int in = Integer.parseInt(input.nextLine(),2);

                String conversion = Integer.toHexString(in);

                System.out.println(ConvertedPL + " ósemkowy:");
                System.out.println(ConvertedEN + " octal system:");

                System.out.println(conversion);

            } else if (inp.matches("3")) {

                System.out.println(BinaryInputPL+" \n"+BinaryInputEN);

                int userInput = Integer.parseInt(input.nextLine(),2);

                String conversion = Integer.toHexString(userInput);

                System.out.println(ConvertedPL + " szesnastkowy:");
                System.out.println(ConvertedEN + " hexadecimal system:");
                System.out.println(Integer.parseInt(conversion, 16));
            }
        } else if (system.matches("3")) {

            System.out.println("(PL)Wybrałeś przeliczenie liczby z systemu ósemkowego, na jaki system chcesz ją przeliczyć?");
            System.out.println("(EN)You choose to convert number from octal number system. To which system you want to convert?");

            System.out.println(" 1.Dziesiętny / Decimal \n 2.Binarny / Binary (0-1) \n 3.Szesnastkowy / Hexadecimal (0-16)");

            String userInput = input.nextLine();

            if (userInput.matches("1")) {

                System.out.println(OctalInputPL+" \n"+OctalInputEN);

                userInput = input.nextLine();

                System.out.println(ConvertedPL + " dziesiętny:");
                System.out.println(ConvertedEN + " decimal system:");

                System.out.println(Integer.toOctalString(Integer.parseInt(userInput)));

            } else if (userInput.matches("2")) {

                System.out.println(OctalInputPL+" \n"+OctalInputEN);

                userInput = input.nextLine();

                int number = Integer.parseInt(userInput);

                System.out.println(ConvertedPL + " binarny:");
                System.out.println(ConvertedEN + " binary system:");
                System.out.println(Integer.toBinaryString(number));

            } else if (userInput.matches("3")) {

                System.out.println(OctalInputPL+" \n"+OctalInputEN);

                userInput = input.nextLine();

                int number = Integer.parseInt(userInput);

                System.out.println(ConvertedPL + " szesnastkowy:");
                System.out.println(ConvertedEN + " hexadecimal system:");
                System.out.println(Integer.toBinaryString(number));

            }
        } else if (system.matches("4")) {

            System.out.println("(PL)Wybrałeś przeliczenie liczby z systemu szesnastkowego, na jaki system chcesz ją przeliczyć?");
            System.out.println("(EN)You choose to convert number from hexadecimal number system. To which system you want to convert?");

            System.out.println(" 1.Dziesiętny / Decimal \n 2.Binarny / Binary (0-1) \n 3.Ósemkowy / Octal (0-8)");

            String in = input.nextLine();

            if (in.matches("1")) {

                System.out.println(HexaInputPL+" \n"+HexaInputEN);

                String conversion = input.nextLine();

                System.out.println(ConvertedPL + " dziesiętny:");
                System.out.println(ConvertedEN + " decimal system:");

                System.out.println(Integer.parseInt(conversion,16));

            } else if (in.matches("2")) {

                System.out.println(HexaInputPL+" \n"+HexaInputEN);

                String conversion = input.nextLine();

                System.out.println(ConvertedPL + " binarny:");
                System.out.println(ConvertedEN + " binary system:");

                System.out.println(Integer.toBinaryString(Integer.parseInt(conversion,16)));

            } else if (in.matches("3")) {

                System.out.println(HexaInputPL+" \n"+HexaInputEN);

                String conversion = input.nextLine();

                System.out.println(ConvertedPL + " ósemkowy");
                System.out.println(ConvertedEN + " octal system");

                System.out.println(Integer.toOctalString(Integer.parseInt(conversion,16)));
            }
        } else {
            System.out.println("Nie ma takiej możliwości / There are no option you choose");
        }
    }
}